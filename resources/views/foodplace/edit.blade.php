@extends('app')

@section('content')
<h4>Ubah </h4>
<form action="{{ route('foodplace.update', $foodplace->id) }}" method="post">
    {{csrf_field()}}
    {{ method_field('PUT') }}
    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
        <label for="name" class="control-label">name</label>
        <input type="text" class="form-control" name="name" placeholder="name" value="{{ $foodplace->name }}">
        @if ($errors->has('name'))
            <span class="help-block">{{ $errors->first('name') }}</span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
        <label for="address" class="control-label">address</label>
        <input type="text" class="form-control" name="address" placeholder="address" value="{{ $foodplace->address }}">
        @if ($errors->has('address'))
            <span class="help-block">{{ $errors->first('address') }}</span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('latitude') ? 'has-error' : '' }}">
        <label for="latitude" class="control-label">latitude</label>
        <input type="text" class="form-control" name="latitude" placeholder="latitude" value="{{ $foodplace->latitude }}">
        @if ($errors->has('latitude'))
            <span class="help-block">{{ $errors->first('latitude') }}</span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('longitude') ? 'has-error' : '' }}">
        <label for="longitude" class="control-label">longitude</label>
        <input type="text" class="form-control" name="longitude" placeholder="longitude" value="{{ $foodplace->longitude }}">
        @if ($errors->has('longitude'))
            <span class="help-block">{{ $errors->first('longitude') }}</span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
        <label for="description" class="control-label">description</label>
        <input type="text" class="form-control" name="description" placeholder="description" value="{{ $foodplace->description }}">
        @if ($errors->has('description'))
            <span class="help-block">{{ $errors->first('description') }}</span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('menu_id') ? 'has-error' : '' }}">
        <label for="menu_id" class="control-label">menu_id</label>
        <input type="text" class="form-control" name="menu_id" placeholder="menu_id" value="{{ $foodplace->menu_id }}">
        @if ($errors->has('menu_id'))
            <span class="help-block">{{ $errors->first('menu_id') }}</span>
        @endif
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-info">Simpan</button>
        <a href="{{ route('foodplace.index') }}" class="btn btn-default">Kembali</a>
    </div>
</form>
@endsection