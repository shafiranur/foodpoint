@extends('app')

@section('content')
    <a href="{{ route('foodplace.create') }}" class="btn btn-info btn-sm">Tambah Tempat Makan</a>
    
    @if ($message = Session::get('message'))
        <div class="alert alert-success martop-sm">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-responsive martop-sm">
        <thead>
            <th>ID</th>
            <th>Name</th>
            <th>Address</th>
        </thead>
        <tbody>
            
            @foreach ($foodplaces as $food)
                <tr>
                    <td>{{ $food->id }}</td>
                    <td><a href="{{ route('foodplace.show', $food->id) }}">{{ $food->name }}</a></td>
                    <td>{{ $food->address }}</td>
                    <td>
                        <form action="{{ route('foodplace.destroy', $food->id) }}" method="post">
                            {{csrf_field()}}
                            {{ method_field('DELETE') }}
                            <a href="{{ route('foodplace.edit', $food->id) }}" class="btn btn-warning btn-sm">Ubah</a>
                            <button type="submit" class="btn btn-danger btn-sm">Hapus</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection